package Homework6;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        Human motherJane = new Human("Jane", "Corleone", 1897);
        Human fatherVito = new Human("Vito", "Corleone", 1891);

        Family familyCorleone = new Family(motherJane, fatherVito);

        Human michael = new Human("Michael", "Corleone", 1977, 90,
                new String[][]{{DayOfWeek.Sunday.name(), "do home work"}, {DayOfWeek.Monday.name(), "go to courses; watch a film"}, {DayOfWeek.Tuesday.name(), "do workout"},
                        {DayOfWeek.Wednesday.name(), "read e-mails"}, {DayOfWeek.Thursday.name(), "do shopping"},
                        {DayOfWeek.Friday.name(), "do household"}, {DayOfWeek.Saturday.name(), "visit grandparents"}});

        michael.setFamily(familyCorleone);
        Pet michaelsPet = new Pet(Species.Dog, "Andy", 1, 60, new String[]{"play, swim in pool, catch a ball"});
        familyCorleone.setPet(michaelsPet);
        michaelsPet.respond();

    }
}




