package Homework6;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    public Family family;
    private Human father;
    private Human mother;
    private Human testch1;
    private Human testch2;
    private Human testch3;
    Pet pet;

    @BeforeEach
    void familyInfo() {
        mother = new Human("Anna", "Barzini", 1891);
        father = new Human("Anna", "Barzini", 1891);
        family = new Family(mother, father);
        Pet emilioPet = new Pet(Species.Cat, "Loona", 2, 30, new String[]{"run, take a nap, hide"});
        family.setPet(emilioPet);
        testch1 = new Human("TestChild1", "BLAH", 1996);
        testch2 = new Human("TestChild2", "BLAHBLAH", 1997);
        testch3 = new Human("TestChild3", "BLAHBLAHBLAH", 1998);
        family.addChild(testch1);
        family.addChild(testch2);
    }

    @Test
    void testToString() {
        String expectedOutput = "Family{mother=Human{name='Anna', surname='Barzini', year=1891, iq=0, schedule=null}, " +
                "father=Human{name='Anna', surname='Barzini', year=1891, iq=0, schedule=null}, " +
                "pet=Cat{nickname='Loona, age=2, trickLevel=30, habits=[run, take a nap, hide]}, " +
                "childrenList=[Human{name='TestChild1', surname='BLAH', year=1996, iq=0, schedule=null}, " +
                "Human{name='TestChild2', surname='BLAHBLAH', year=1997, iq=0, schedule=null}]}";
        assertEquals(expectedOutput, family.toString());
    }

    @Test
    void addChild() {
        family.addChild(testch3);
        assertTrue(family.getChildrenList().contains(testch3));
    }

    @Test
    void deleteExistingObjectChild() {
        assertTrue(family.deleteChild(testch1));
        assertFalse(family.getChildrenList().contains(testch1));
    }

    @Test
    void deleteNonExistingObjectChild() {
        assertFalse(family.deleteChild(testch3));
    }

    @Test
    void deleteExistingIndexChild() {
        assertTrue(family.deleteChild(0));
        assertFalse(family.getChildrenList().contains(testch1));
    }

    @Test
    void deleteNonExistingIndexChild() {
        assertFalse(family.deleteChild(10));
    }


    @Test
    void countFamily() {
        int result = family.countFamily();
        assertEquals(4, result);
    }
}